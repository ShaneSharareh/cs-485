﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    public float speed;
    public Text countText;
    private Rigidbody rb;
    private int count;
    public Text winText;
    void Start()
    {
        count = 0;
        rb = GetComponent<Rigidbody>();
        setCountText();
        winText.text = "";
    }
    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(moveHorizontal, 0, moveVertical);
        rb.AddForce(movement * speed);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
           count++;
            setCountText();
        }
        
    }
    void setCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count>= 11){
            winText.text = "You Win!";
        }
    }
}
