﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class enemy : MonoBehaviour
{
    public Transform goal;
    Transform tr_Player;//player transformation
    float f_RotSpeed = 3.0f;
    float f_MoveSpeed = 5.0f;
    public GameObject[] respawns;
    private int enemyCount =0;
    private PlayerController playerScript;
    private AudioSource coin;
    private AudioSource fail;
    public Text enemyCountText;
    NavMeshAgent agent;
    void Start()
    {
        agent= GetComponent< NavMeshAgent > ();
        
        playerScript = GameObject.Find("Player").GetComponent<PlayerController>();

        GameObject pickups = GameObject.FindGameObjectWithTag("pickupParent");
        GameObject manager = GameObject.FindGameObjectWithTag("radio");
        coin = pickups.GetComponent<AudioSource>();
        fail = manager.GetComponent<AudioSource>();
        tr_Player = GameObject.FindGameObjectWithTag("Pick Up").transform;//when the enemy is created store position of player, assigns position of player

    }
    public int getEnemyCount()
    {
        return enemyCount;
    }

    /*This method finds out which pickup cube is closest by putting all clones inside an array
     *First cube is compared with infinity
     * The next one is compared with the position of the last cube
     * returns the closest cube
     * If no cube, return null
     */
    GameObject FindClosestPickup()
    {
        GameObject[] pickup_Prefab;
        pickup_Prefab = GameObject.FindGameObjectsWithTag("Pick Up");
        GameObject closest = null;
        float distance = Mathf.Infinity;// initially measures distance in terms of infinity
        Vector3 position = this.transform.position;
        foreach (GameObject pickup in pickup_Prefab)//runs through each pick up prefab and measures which one is closest
        {
            Vector3 diff = pickup.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)//if the pickups position is closer
            {
                closest = pickup;// assign that pickup with closest
                distance = curDistance;// set its current distance as overall distance and go through the array again to see if any pick up is closer
            }
        }

        return closest;
    }
    

    // Update is called once per frame
    void Update()
    {
        


        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
        
        GameObject tempPickup = FindClosestPickup();// returns closest cube.
        if (tempPickup != null && (tempPickup.active == true))// if there are still pickup cubes, and if the cube is active move toward that position
        {

            tr_Player = tempPickup.transform;// get tempPickups transformation 
            /*look at player*/
            agent.destination = tr_Player.position;
            transform.rotation = Quaternion.Slerp(transform.rotation,
                Quaternion.LookRotation(tr_Player.position - transform.position), f_RotSpeed * Time.deltaTime);//turns rotation of enemy object toward the pick up from and to position, and rotation speed based on time
                                                                                                               /* move at Player*/
            transform.position += transform.forward * f_MoveSpeed * Time.deltaTime;//based on time, it moves forward
        }

        else//If blocks in range don't exist, determine who collected the most
        {
            
            
            if (playerScript.count >= enemyCount)
                {

                playerScript.winText.text = "You Win!";
                }
               else
               {

                fail.Play();
                playerScript.winText.text = "Game Over!";
                }


            }
        }
        
    
    /*When enemy collects cube, add to count*/
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
              
            other.gameObject.SetActive(false);
            enemyCount++;
            enemyCountText.text = "Enemy Count: " + enemyCount.ToString();
            coin.Play();


        }


    }
}
