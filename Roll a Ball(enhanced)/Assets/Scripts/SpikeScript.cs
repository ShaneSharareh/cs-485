﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class SpikeScript : MonoBehaviour {
    private PlayerController playerScript;
    private AudioSource fail;
    // Use this for initialization
    void Start () {
        playerScript = GameObject.Find("Player").GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update () {
		
	}

/*When Player hits cube, deactivate*/
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
          
            other.gameObject.SetActive(false);
          
        }
    }
    
}
