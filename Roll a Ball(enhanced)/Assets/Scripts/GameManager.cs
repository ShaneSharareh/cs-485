﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameManager : MonoBehaviour {
    /*
     *GameManager handels game 
     */
    AudioSource fail;
    PlayerController playerScript;
    GameObject player;
    bool gameOver= false;
    public Text countDown;
    private int count = 5;
    // Use this for initialization
    void Start () {
        playerScript = GameObject.Find("Player").GetComponent<PlayerController>();//retrieve player script
        fail = GetComponent<AudioSource>();//retrieve game over script
        player = GameObject.FindGameObjectWithTag("Player");
      
    }
   
    // Update is called once per frame
    void Update()
    {
        
            if (!gameOver)
            {
                if (player.transform.position.y <= -10)
                {
                    playerScript.winText.text = "Game Over!";
                    fail.Play();//plays game over sound sound
                                
                    player.SetActive(false);
                    gameOver = true;
                }
                else if (player.active == false)
                {

                    fail.Play();//plays sound
                                
                playerScript.winText.text = "Ouch! Game Over!";

                gameOver = true;
                }

            }
            if (Input.GetKey(KeyCode.Escape))
            {
                Application.LoadLevel(0);//if you press escape, takes you back to main menu

            }
        
    }
    
}
