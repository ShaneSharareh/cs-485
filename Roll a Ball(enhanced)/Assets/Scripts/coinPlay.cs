﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coinPlay : MonoBehaviour {
    AudioSource coin;
   
    // Use this for initialization
    void Start () {
        coin = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update () {
		
	}
    void OnCollisionEnter(Collision other)
    {

        if (other.gameObject.CompareTag("Player"))
        {

            coin.Play();
        }
    }
}
