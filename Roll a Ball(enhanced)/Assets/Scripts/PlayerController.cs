﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    public float speed;
    public Text countText;
    private Rigidbody rb;
    public int count;
    public Text winText;
    private  AudioSource coin;
    private AudioSource fail;

    void Start()
    {
        count = 0;
        GameObject pickups = GameObject.FindGameObjectWithTag("pickupParent");
        coin = pickups.GetComponent<AudioSource>();
        GameObject radio = GameObject.FindGameObjectWithTag("radio");
        fail = radio.GetComponent<AudioSource>();
        count = 0;
        rb = GetComponent<Rigidbody>();
        setCountText();
        winText.text = "";
        
    }
    
    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(moveHorizontal, 0, moveVertical);
        rb.AddForce(movement * speed);
        
        

    }
    


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
           count++;
            coin.Play();
            setCountText();
        }
       else if (other.gameObject.CompareTag("spike")){
            gameObject.SetActive(false);
            winText.text = "Ouch! Game Over";
        }

    }
    void setCountText()
    {
        
        countText.text = "Count: " + count.ToString();
       
        
    }
    
}
